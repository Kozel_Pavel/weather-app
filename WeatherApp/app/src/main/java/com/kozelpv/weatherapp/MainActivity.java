package com.kozelpv.weatherapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.kozelpv.weatherapp.constant.Constant;
import com.kozelpv.weatherapp.fragments.AddCityDialogFragment;
import com.kozelpv.weatherapp.fragments.CityListFragment;
import com.kozelpv.weatherapp.fragments.ForecastListFragment;
import com.kozelpv.weatherapp.repository.database.City;

public class MainActivity extends AppCompatActivity implements AddCityDialogFragment.AddCityDialogListener,
        ForecastListFragment.ForecastListFragmentListener,
        CityListFragment.CityListFragmentListener {

    public static final String NAME_KEY = "CITY_NAME";
    private static final int LOCATION_PERMISSION_CODE = 18000;

    private LocationManager locationManager;
    private Location location = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        doSomeOperationWithFragment(Constant.ADD, true,
                ForecastListFragment.newInstance(), ForecastListFragment.FRAGMENT_TAG);
    }

    private void doSomeOperationWithFragment(int someOperation, boolean addInStack, Fragment fragment, String FRAGMENT_TAG) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        switch (someOperation) {
            case Constant.ADD:
                fragmentTransaction.add(R.id.fragmentContainer, fragment, FRAGMENT_TAG);
                break;
            case Constant.REPLACE:
                fragmentTransaction.replace(R.id.fragmentContainer, fragment, FRAGMENT_TAG);
                break;
            case Constant.REMOVE:
                fragmentTransaction.remove(fragment);
                break;
            default:
                break;
        }
        if (addInStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onComplite(String cityName) {
        CityListFragment cityListFragment = (CityListFragment) getSupportFragmentManager().findFragmentByTag(CityListFragment.FRAGMENT_TAG);

        if (cityListFragment != null) {
            cityListFragment.addToViewModel(cityName);
        }
    }

    @Override
    public void onFloatingButtonClick() {
        doSomeOperationWithFragment(Constant.ADD, true, CityListFragment.newInstance(), CityListFragment.FRAGMENT_TAG);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCurrentPositionButtonClick() {
        askLocationPermission();
        if (location != null) {
            double currentPositionLatitude = location.getLatitude();
            double currentPositionLongitude = location.getLongitude();

            ForecastListFragment forecastListFragment = (ForecastListFragment) getSupportFragmentManager().findFragmentByTag(ForecastListFragment.FRAGMENT_TAG);
            forecastListFragment.setForecastForCurrentPosition(currentPositionLatitude, currentPositionLongitude);
        }
    }

    @Override
    public void onCitySelect(City city) {
        getSupportFragmentManager().popBackStack();
        String cityName = city.getCityName();
        ForecastListFragment forecastListFragment = (ForecastListFragment) getSupportFragmentManager().findFragmentByTag(ForecastListFragment.FRAGMENT_TAG);
        forecastListFragment.setCityName(cityName);
    }

    @Override
    public void onReturnClick() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_CODE &&
                grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            location = getLastBestLocation();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            location = location;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void askLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 10, locationListener);
            location = getLastBestLocation();
        } else {
            String[] locationPermissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
            requestPermissions(locationPermissions, LOCATION_PERMISSION_CODE);
        }
    }

    @SuppressLint("MissingPermission")
    private Location getLastBestLocation() {
        Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        long GPSLocationTime = 0;
        if (locationGPS != null) {
            GPSLocationTime = locationGPS.getTime();
        }

        long NetLocationTime = 0;

        if (locationGPS != null) {
            NetLocationTime = locationNet.getTime();
        }

        if (GPSLocationTime - NetLocationTime > 0) {
            return locationGPS;
        } else {
            return locationNet;
        }
    }
}
