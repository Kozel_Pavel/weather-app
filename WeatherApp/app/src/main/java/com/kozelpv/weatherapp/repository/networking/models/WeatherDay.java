package com.kozelpv.weatherapp.repository.networking.models;

import android.icu.util.Calendar;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.google.gson.annotations.SerializedName;
import com.kozelpv.weatherapp.constant.Constant;

import java.util.List;

public class WeatherDay {
    public class WeatherTemp {
        Double temp;
        Double temp_min;
        Double temp_max;
    }

    public class WeatherDescription {
        String description;
        String icon;
    }

    @SerializedName("main")
    private WeatherTemp temp;

    @SerializedName("weather")
    private List<WeatherDescription> weatherList;

    @SerializedName("name")
    private String name;

    @SerializedName("dt")
    private long timestamp;

    public WeatherDay(WeatherTemp temp, List<WeatherDescription> weatherList) {
        this.temp = temp;
        this.weatherList = weatherList;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Calendar getDate() {
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(timestamp * 1000);
        return date;
    }

    public String getTemp() {
        return String.valueOf(temp.temp);
    }

    public String getTempMin() {
        return String.valueOf(temp.temp_min);
    }

    public String getTempMax() {
        return String.valueOf(temp.temp_max);
    }

    public String getTempInteger() {
        return String.valueOf(temp.temp.intValue());
    }

    public String getTempWithDegree() {
        return String.valueOf(temp.temp.intValue()) + "\u00B0";
    }

    public String getName() {
        return name;
    }

    public String getIcon() {
        return weatherList.get(0).icon;
    }

    public String getDescription() {
        return weatherList.get(0).description;
    }

    public String getIconUrl() {
        return String.format(Constant.CURRENT_ICON_URL, getIcon());
    }
}
