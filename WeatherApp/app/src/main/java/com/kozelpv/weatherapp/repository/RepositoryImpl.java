package com.kozelpv.weatherapp.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.kozelpv.weatherapp.BuildConfig;
import com.kozelpv.weatherapp.constant.Constant;
import com.kozelpv.weatherapp.repository.database.City;
import com.kozelpv.weatherapp.repository.database.CityDao;
import com.kozelpv.weatherapp.repository.database.CityRoomDatabase;
import com.kozelpv.weatherapp.repository.networking.ApiBuilder;
import com.kozelpv.weatherapp.repository.networking.ApiService;
import com.kozelpv.weatherapp.repository.networking.models.WeatherDay;
import com.kozelpv.weatherapp.repository.networking.models.WeatherForecast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;

// Abstract class for use request to DB,servers, s.e.
public class RepositoryImpl implements RepositoryDataBaseCity, RepositoryNetworking {

    private static final String API_KEY = BuildConfig.API_KEY;
    private static final String UNITS_METRIC = Constant.UNITS_METRIC;

    private CityDao cityDao;
    private LiveData<List<City>> allCity;
    private static RepositoryImpl thisRepository;

    private ApiService apiService;

    private RepositoryImpl(Application application) {
        CityRoomDatabase db = CityRoomDatabase.getDatabase(application);
        this.cityDao = db.getCityDao();

        Retrofit retrofit = ApiBuilder.getRetrofitClient(application.getApplicationContext());
        apiService = retrofit.create(ApiService.class);
    }

    public static RepositoryImpl getRepositoryInstance(Application application) {
        if (thisRepository == null) {
            thisRepository = new RepositoryImpl(application);
        }
        return thisRepository;
    }

    @Override
    public LiveData<List<City>> getAllCity() {
        if (allCity == null) {
            allCity = cityDao.getAllCity();
        }
        return allCity;
    }

    @Override
    public void insert(final City city) {
        CityRoomDatabase.databaseWriteExecutor.execute(() -> cityDao.insert(city));
    }

    @Override
    public void deleteById(final int id) {
        CityRoomDatabase.databaseWriteExecutor.execute(() -> cityDao.deleteCityDyId(id));
    }

    @Override
    public Call<WeatherDay> getTodayWeatherByCityName(String cityName) {
        return apiService.getTodayWeatherByCityName(cityName, API_KEY, UNITS_METRIC);
    }

    @Override
    public Call<WeatherForecast> getForecastByCityName(String cityName) {
        return apiService.getForecastByCityName(cityName, API_KEY, UNITS_METRIC);
    }

    @Override
    public Call<WeatherDay> getTodayWeatherByCoordinates(Double latitude, Double longitude) {
        return apiService.getTodayWeatherByCoordinates(latitude, longitude, API_KEY, UNITS_METRIC);
    }

    @Override
    public Call<WeatherForecast> getForecastByCoordinates(Double latitude, Double longitude) {
        return apiService.getForecastByCoordinates(latitude, longitude, API_KEY, UNITS_METRIC);
    }
}
