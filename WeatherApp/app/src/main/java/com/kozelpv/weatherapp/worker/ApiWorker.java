package com.kozelpv.weatherapp.worker;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.kozelpv.weatherapp.constant.Constant;
import com.kozelpv.weatherapp.repository.RepositoryImpl;
import com.kozelpv.weatherapp.repository.networking.models.WeatherDay;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

public class ApiWorker extends Worker {

    private RepositoryImpl repository;

    public ApiWorker(@NonNull Context context,
                     @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        this.repository = RepositoryImpl.getRepositoryInstance((Application) getApplicationContext());
    }

    public static final String TAG = ApiWorker.class.getSimpleName();

    @NonNull
    @Override
    public Result doWork() {
        //ToDo: add method logic here
        Context appContext = getApplicationContext();

        String cityName = getInputData().getString(Constant.APP_PREFERENCES_NAME);
        Call<WeatherDay> call = repository.getTodayWeatherByCityName(cityName);
        try {
            Response<WeatherDay> response = call.execute();
            if (response.isSuccessful()) {
                WeatherDay weatherDay = response.body();
                StringBuilder messageBuilder = new StringBuilder();
                messageBuilder.append(weatherDay.getName());
                messageBuilder.append(weatherDay.getTempWithDegree());
                messageBuilder.append(weatherDay.getDescription());
                String notificationMessage = messageBuilder.toString();
                WorkerUtils.makeStatusNotification(notificationMessage, appContext);
                return Result.success();
            }
            return Result.failure();
        } catch (IOException e) {
            e.printStackTrace();
            return Result.failure();
        }
    }
}
