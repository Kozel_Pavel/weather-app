package com.kozelpv.weatherapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kozelpv.weatherapp.R;
import com.kozelpv.weatherapp.repository.database.City;
import com.kozelpv.weatherapp.fragments.CityListFragment;

import java.util.List;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {

    private final LayoutInflater mInflater;
    private List<City> cityList;
    private CityListFragment.CityListFragmentListener listener;

    public CityAdapter(Context context, @NonNull CityListFragment.CityListFragmentListener listener) {
        mInflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @NonNull
    @Override
    public CityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recycler_view_city, parent, false);
        return new CityViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CityViewHolder holder, int position) {
        if (cityList != null) {
            final City city = cityList.get(position);
            holder.number.setText((position + 1) + ")");
            holder.cityName.setText(city.getCityName());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onCitySelect(city);
                    }
                }
            });
        } else {
            holder.cityName.setText(R.string.no_city);
        }
    }

    public void updateCityList(List<City> cities) {
        this.cityList = cities;
        notifyDataSetChanged();
    }

    public int getCityIdFromListInAdapter(int id) {
        return cityList.get(id).getCityId();
    }

    @Override
    public int getItemCount() {
        return cityList != null ? cityList.size() : 0;
    }

    class CityViewHolder extends RecyclerView.ViewHolder {
        private TextView number;
        private TextView cityName;

        private CityViewHolder(@NonNull View itemView) {
            super(itemView);
            number = itemView.findViewById(R.id.number);
            cityName = itemView.findViewById(R.id.cityName);
        }
    }
}
