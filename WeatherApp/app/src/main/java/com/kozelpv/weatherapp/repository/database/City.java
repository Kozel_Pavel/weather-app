package com.kozelpv.weatherapp.repository.database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "city")
public class City {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "city_id")
    private int cityId;

    @ColumnInfo(name = "city_name")
    private String cityName;

    public City(String cityName) {
        this.cityName = cityName;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getCityId() {
        return cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @NonNull
    @Override
    public String toString() {
        return "City{" +
                "id=" + cityId + "\'" +
                "city=" + cityName + "}";
    }
}
