package com.kozelpv.weatherapp;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.kozelpv.weatherapp.repository.database.City;
import com.kozelpv.weatherapp.repository.RepositoryImpl;

import java.util.List;

public class CityViewModel extends AndroidViewModel {
    private RepositoryImpl repository;
    private LiveData<List<City>> allCity;

    public CityViewModel(@NonNull Application application) {
        super(application);
        repository = RepositoryImpl.getRepositoryInstance(application);
        allCity = repository.getAllCity();
    }

    public LiveData<List<City>> getAllCity() {
        return allCity;
    }

    public void insert(City city) {
        repository.insert(city);
    }

    public void deleteById(int id) {
        repository.deleteById(id);
    }
}
