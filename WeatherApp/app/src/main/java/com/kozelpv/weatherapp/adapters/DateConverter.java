package com.kozelpv.weatherapp.adapters;

import android.icu.util.Calendar;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.text.SimpleDateFormat;

public class DateConverter {
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String convert(Calendar calendar) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm - MM/dd/yyyy");
        return sdf.format(calendar.getTime());
    }
}

