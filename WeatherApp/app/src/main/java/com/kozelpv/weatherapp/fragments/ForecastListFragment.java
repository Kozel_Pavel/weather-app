package com.kozelpv.weatherapp.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.kozelpv.weatherapp.ForecastViewModel;
import com.kozelpv.weatherapp.MainActivity;
import com.kozelpv.weatherapp.R;
import com.kozelpv.weatherapp.adapters.ForecastAdapter;
import com.kozelpv.weatherapp.databinding.FragmentForecastListBinding;
import com.kozelpv.weatherapp.repository.networking.models.WeatherDay;
import com.kozelpv.weatherapp.repository.networking.models.WeatherForecast;

public class ForecastListFragment extends Fragment {
    public static final String FRAGMENT_TAG = ForecastListFragment.class.getName();

    public static ForecastListFragment newInstance() {
        return new ForecastListFragment();
    }

    public interface ForecastListFragmentListener {
        void onFloatingButtonClick();

        void onCurrentPositionButtonClick();
    }

    private static String cityName;

    private ForecastListFragmentListener listener;

    private ForecastViewModel forecastViewModel;

    private Toolbar toolbar;

    private Button currentPositionButton;
    private FloatingActionButton floatingActionButton;

    private RecyclerView recyclerView;
    private ForecastAdapter forecastAdapter;

    private SharedPreferences sharedPreferences;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (ForecastListFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() +
                    "must implements ForecastListFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentForecastListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forecast_list, container, false);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        View view = binding.getRoot();
        forecastViewModelInit();
        binding.setForecastViewModel(forecastViewModel);

        toolbar = view.findViewById(R.id.toolbar);

        floatingActionButton = view.findViewById(R.id.changeCity);
        currentPositionButton = view.findViewById(R.id.currentPositionButton);

        recyclerView = view.findViewById(R.id.forecastView);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        recyclerViewInit();
        setToolbarSetting();
        loadCityNameFromPreferences();
        getForecastByCityName(cityName);

        floatingActionButton.setOnClickListener(v -> listener.onFloatingButtonClick());

        currentPositionButton.setOnClickListener(v -> listener.onCurrentPositionButtonClick());

        super.onViewCreated(view, savedInstanceState);
    }

    private void forecastViewModelInit() {
        forecastViewModel = ViewModelProviders.of(this).get(ForecastViewModel.class);

        forecastViewModel.getWeatherDayLiveData().observe(getViewLifecycleOwner(),
                weatherDay -> updateWeatherDay(weatherDay));
        forecastViewModel.getWeatherForecastLiveData().observe(getViewLifecycleOwner(),
                weatherForecasts -> updateForecastAdapter(weatherForecasts));

    }

    private void updateForecastAdapter(WeatherForecast weatherForecast) {
        forecastAdapter.setDataInAdapter(weatherForecast);
    }

    private void updateWeatherDay(WeatherDay weatherDay) {
        // ???
    }

    private void getForecastByCityName(String city) {
        forecastViewModel.getTodayWeatherByCityName(city);
        forecastViewModel.getForecastByCityName(city);
    }

    private void recyclerViewInit() {
        forecastAdapter = new ForecastAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(forecastAdapter);
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
        updateFragment();
    }

    public void setForecastForCurrentPosition(double latitude, double longitude) {
        getForecastByCoordinate(latitude, longitude);
    }

    private void getForecastByCoordinate(double latitude, double longitude) {
        forecastViewModel.getTodayWeatherByCoordinates(latitude, longitude);
        forecastViewModel.getForecastByCoordinates(latitude, longitude);
    }

    private void updateFragment() {
        getForecastByCityName(cityName);
        saveCityNameInPreferences();
    }

    private void saveCityNameInPreferences() {
        sharedPreferences = getActivity().getPreferences(getContext().MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(MainActivity.NAME_KEY, cityName);
        editor.apply();
        Toast.makeText(getContext(), "Text saved", Toast.LENGTH_SHORT).show();
    }

    private void loadCityNameFromPreferences() {
        sharedPreferences = getActivity().getPreferences(getContext().MODE_PRIVATE);
        cityName = sharedPreferences.getString(MainActivity.NAME_KEY, "NO SAVE CITY");
        Toast.makeText(getContext(), "Text loaded", Toast.LENGTH_SHORT).show();
    }

    private void setToolbarSetting() {
        AppCompatActivity thisActivity = (AppCompatActivity) getActivity();
        thisActivity.setSupportActionBar(toolbar);
        thisActivity.getSupportActionBar().setTitle(R.string.city_info);
    }
}
