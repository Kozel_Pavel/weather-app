package com.kozelpv.weatherapp.repository.networking;

import com.kozelpv.weatherapp.repository.networking.models.WeatherDay;
import com.kozelpv.weatherapp.repository.networking.models.WeatherForecast;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @GET("weather")
    Call<WeatherDay> getTodayWeatherByCityName(@Query("q") String cityName,
                                               @Query("appid") String appid,
                                               @Query("units") String units);   //standard, metric or imperial

    @GET("forecast")
    Call<WeatherForecast> getForecastByCityName(@Query("q") String cityName,
                                                @Query("appid") String apiKey,
                                                @Query("units") String units);   //standard, metric or imperial

    @GET("weather")
    Call<WeatherDay> getTodayWeatherByCoordinates(@Query("lat") Double latitude,
                                                  @Query("lon") Double longitude,
                                                  @Query("appid") String apiKey,
                                                  @Query("units") String units);   //standard, metric or imperial

    @GET("forecast")
    Call<WeatherForecast> getForecastByCoordinates(@Query("lat") Double latitude,
                                                   @Query("lon") Double longitude,
                                                   @Query("appid") String apiKey,
                                                   @Query("units") String units);   //standard, metric or imperial

}
