package com.kozelpv.weatherapp.fragments;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.kozelpv.weatherapp.CityViewModel;
import com.kozelpv.weatherapp.R;
import com.kozelpv.weatherapp.repository.database.City;
import com.kozelpv.weatherapp.adapters.CityAdapter;

import java.util.List;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class CityListFragment extends Fragment {

    public static final String FRAGMENT_TAG = CityListFragment.class.getName();

    public interface CityListFragmentListener {
        void onReturnClick();

        void onCitySelect(City city);
    }

    private FloatingActionButton addCityButton;

    private Toolbar toolbar;

    private CityListFragmentListener cityListFragmentListener;

    private RecyclerView recyclerView;
    private CityAdapter cityAdapter;
    private CityViewModel cityViewModel;

    public static CityListFragment newInstance() {
        return new CityListFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            cityListFragmentListener = (CityListFragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() +
                    "must implements CityListFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        cityListFragmentListener = null;
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city_list, container, false);

        toolbar = view.findViewById(R.id.toolbar);

        addCityButton = view.findViewById(R.id.addCityButton);

        recyclerView = view.findViewById(R.id.cityRecyclerView);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setToolbarSetting();
        recyclerViewInit();
        cityViewModelInit();

        addCityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddCityDialog();
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    public void showAddCityDialog() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new AddCityDialogFragment();
        dialog.show(getActivity().getSupportFragmentManager(), "AddCityDialogFragment");
    }

    public void addToViewModel(String cityName) {
        cityViewModel.insert(new City(cityName));
    }

    public void deleteFromViewModel(int id) {
        cityViewModel.deleteById(id);
    }

    private void recyclerViewInit() {
        cityAdapter = new CityAdapter(getContext(), cityListFragmentListener);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(cityAdapter);
        setItemTouchHelperToRecyclerView(recyclerView);
    }

    private void cityViewModelInit() {
        cityViewModel = ViewModelProviders.of(this).get(CityViewModel.class);
        cityViewModel.getAllCity().observe(getViewLifecycleOwner(), new Observer<List<City>>() {
            @Override
            public void onChanged(List<City> cities) {
                cityAdapter.updateCityList(cities);
            }
        });
    }

    private void setToolbarSetting() {
        AppCompatActivity thisActivity = (AppCompatActivity) getActivity();
        thisActivity.setSupportActionBar(toolbar);

        ActionBar actionBar = thisActivity.getSupportActionBar();
        actionBar.setTitle(R.string.city_list);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void setItemTouchHelperToRecyclerView(RecyclerView recyclerView) {
        ItemTouchHelper.SimpleCallback callback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                try {
                    int position = viewHolder.getAdapterPosition();
                    final int cityId = cityAdapter.getCityIdFromListInAdapter(position);
                    Snackbar snackbar = Snackbar.make(viewHolder.itemView, R.string.delete_city_info, Snackbar.LENGTH_LONG);
                    snackbar.show();
                    deleteFromViewModel(cityId);
                } catch (Exception e) {
                    Log.e("MainActivity", e.getMessage());
                }
            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView,
                                    @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY,
                                    int actionState, boolean isCurrentlyActive) {
                new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addSwipeRightBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorRed))
                        .addSwipeRightActionIcon(R.drawable.ic_delete_sweep_black_24dp)
                        .addSwipeRightLabel(getString(R.string.delete_city))
                        .create()
                        .decorate();
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.confirm_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                cityListFragmentListener.onReturnClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
