package com.kozelpv.weatherapp.constant;

public class Constant {
    public static final String CURRENT_WEATHER_URL = "https://api.openweathermap.org/data/2.5/weather?q=%s&APPID=%s&units=metric";
    public static final String CURRENT_FUTURE_WEATHER_URL = "https://api.openweathermap.org/data/2.5/forecast?q=%s&APPID=%s&units=metric";
    public static final String CURRENT_ICON_URL = "https://openweathermap.org/img/wn/%s@2x.png";

    public static final int ADD = 1;
    public static final int REPLACE = 2;
    public static final int REMOVE = 3;

    // units for openweathermap.api call
    public static final String UNITS_STANDARD = "standard";
    public static final String UNITS_METRIC = "metric";
    public static final String UNITS_IMPERIAL = "imperial";

    // Notification Channel constants

    // Name of Notification Channel for verbose notifications of background work
    public static final CharSequence VERBOSE_NOTIFICATION_CHANNEL_NAME =
            "Verbose WorkManager Notifications";
    public static String VERBOSE_NOTIFICATION_CHANNEL_DESCRIPTION =
            "Shows notifications whenever work starts";
    public static final CharSequence NOTIFICATION_TITLE = "WorkRequest Starting";
    public static final String CHANNEL_ID = "VERBOSE_NOTIFICATION";
    public static final int NOTIFICATION_ID = 1;

    // Constants for APP sharedPreferences
    public static final String APP_PREFERENCES = "my settings";

    // variables filed for preference
    public static final String APP_PREFERENCES_NAME = "City name";     // city name
    public static final String APP_PREFERENCES_UPDATE_TIME = "update time";  // update time in hour
    public static final String APP_PREFERENCES_INTERNET_CHECK = "internet check";  // internet check

    private Constant() {
    }
}
