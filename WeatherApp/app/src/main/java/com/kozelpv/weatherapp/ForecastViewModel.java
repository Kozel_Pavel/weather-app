package com.kozelpv.weatherapp;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.kozelpv.weatherapp.repository.RepositoryImpl;
import com.kozelpv.weatherapp.repository.networking.models.WeatherDay;
import com.kozelpv.weatherapp.repository.networking.models.WeatherForecast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForecastViewModel extends AndroidViewModel {

    private RepositoryImpl repository;
    private MutableLiveData<WeatherForecast> weatherForecastLiveData = new MutableLiveData<>();
    private MutableLiveData<WeatherDay> weatherDayLiveData = new MutableLiveData<>();

    public ForecastViewModel(@NonNull Application application) {
        super(application);
        this.repository = RepositoryImpl.getRepositoryInstance(application);
    }

    public void getTodayWeatherByCityName(String cityName) {
        Call<WeatherDay> call = repository.getTodayWeatherByCityName(cityName);

        call.enqueue(new Callback<WeatherDay>() {
            @Override
            public void onResponse(Call<WeatherDay> call, Response<WeatherDay> response) {
                WeatherDay weatherDay = response.body();
                weatherDayLiveData.postValue(weatherDay);
            }

            @Override
            public void onFailure(Call<WeatherDay> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getForecastByCityName(String cityName) {
        Call<WeatherForecast> call = repository.getForecastByCityName(cityName);

        call.enqueue(new Callback<WeatherForecast>() {
            @Override
            public void onResponse(Call<WeatherForecast> call, Response<WeatherForecast> response) {
                WeatherForecast weatherForecast = response.body();
                weatherForecastLiveData.postValue(weatherForecast);
            }

            @Override
            public void onFailure(Call<WeatherForecast> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getTodayWeatherByCoordinates(Double latitude, Double longitude) {
        Call<WeatherDay> call = repository.getTodayWeatherByCoordinates(latitude, longitude);

        call.enqueue(new Callback<WeatherDay>() {
            @Override
            public void onResponse(Call<WeatherDay> call, Response<WeatherDay> response) {
                WeatherDay weatherDay = response.body();
                weatherDayLiveData.postValue(weatherDay);
            }

            @Override
            public void onFailure(Call<WeatherDay> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getForecastByCoordinates(Double latitude, Double longitude) {
        Call<WeatherForecast> call = repository.getForecastByCoordinates(latitude, longitude);

        call.enqueue(new Callback<WeatherForecast>() {
            @Override
            public void onResponse(Call<WeatherForecast> call, Response<WeatherForecast> response) {
                WeatherForecast weatherForecast = response.body();
                weatherForecastLiveData.postValue(weatherForecast);
            }

            @Override
            public void onFailure(Call<WeatherForecast> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public LiveData<WeatherForecast> getWeatherForecastLiveData() {
        return weatherForecastLiveData;
    }

    public LiveData<WeatherDay> getWeatherDayLiveData() {
        return weatherDayLiveData;
    }
}
