package com.kozelpv.weatherapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.kozelpv.weatherapp.constant.Constant;
import com.kozelpv.weatherapp.worker.ApiWorker;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class WorkManagerSettingsActivity extends AppCompatActivity {

    @BindView(R.id.cityNameEditText)
    EditText cityNameEditText;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    @OnClick({R.id.oneHour, R.id.threeHour, R.id.twentyHour})
    public void onRadioButtonClicked(RadioButton radioButton) {
        boolean checked = radioButton.isChecked();

        switch (radioButton.getId()) {
            case R.id.oneHour:
                if (checked) {
                    hour = 1;
                }
                break;
            case R.id.threeHour:
                if (checked) {
                    hour = 3;
                }
                break;
            case R.id.twentyHour:
                if (checked) {
                    hour = 12;
                }
                break;
        }

        checkRadioButtonIndex = radioGroup.getCheckedRadioButtonId();
    }

    @OnClick({R.id.saveSettingButton, R.id.enableWorkerButton, R.id.disableWorkerButton})
    public void onButtonClick(Button button) {
        switch (button.getId()) {
            case R.id.saveSettingButton:
                saveNewSettingInPreference();
                break;
            case R.id.enableWorkerButton:
                saveNewSettingInPreference();
                break;
            case R.id.disableWorkerButton:
                saveNewSettingInPreference();
                break;
        }
    }

    private String cityName;
    private SharedPreferences settingsPreferences;
    private WorkManager workManager;
    private int checkRadioButtonIndex;
    private int hour = 1;
    private boolean internetCheck = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_work);

        settingsPreferences = getSharedPreferences(Constant.APP_PREFERENCES, Context.MODE_PRIVATE);
        getSettingsFromPreference();
        workManager = WorkManager.getInstance(getApplicationContext());

        Switch internetSwitch = findViewById(R.id.internetSwitch);
        internetSwitch.setChecked(internetCheck);
        internetSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> internetCheck = isChecked);
    }

    private void doCallToApi() {
        //ToDo: add method logic by workManager
        PeriodicWorkRequest.Builder myWorkRequestBuilder = new PeriodicWorkRequest.Builder(ApiWorker.class,
                hour * 60 - 5, TimeUnit.MINUTES,
                hour, TimeUnit.HOURS)
                .setInputData(createInputDataForWorkRequest());

        if (internetCheck) {
            Constraints constraint = new Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.CONNECTED)
                    .build();

            myWorkRequestBuilder.setConstraints(constraint);
        }

        PeriodicWorkRequest myWorkRequest = myWorkRequestBuilder.build();
        workManager.enqueue(myWorkRequest);
    }

    private Data createInputDataForWorkRequest() {
        Data.Builder dataBuilder = new Data.Builder();
        //ToDo: realize normal logic
        if (cityName != null) {
            dataBuilder.putString(Constant.APP_PREFERENCES_NAME, cityName);
        }
        return dataBuilder.build();
    }

    private void saveNewSettingInPreference() {
        SharedPreferences.Editor editor = settingsPreferences.edit();
        //ToDO: add some parameters
        cityName = cityNameEditText.getText().toString();
        editor.putString(Constant.APP_PREFERENCES_NAME, cityName);
        editor.putInt(Constant.APP_PREFERENCES_UPDATE_TIME, hour);
        editor.putBoolean(Constant.APP_PREFERENCES_INTERNET_CHECK, internetCheck);
        editor.apply();
    }

    private void getSettingsFromPreference() {
        //ToDo: get all parameters from preference to my layout and activity
        cityName = settingsPreferences.getString(Constant.APP_PREFERENCES_NAME, "");
        cityNameEditText.setText(cityName);

        hour = settingsPreferences.getInt(Constant.APP_PREFERENCES_UPDATE_TIME, 0);
        switch (hour) {
            case 1:
                checkRadioButtonIndex = 0;
                break;
            case 3:
                checkRadioButtonIndex = 1;
                break;
            case 12:
                checkRadioButtonIndex = 2;
                break;
        }
        RadioButton savedCheckRadioButton = (RadioButton) radioGroup.getChildAt(checkRadioButtonIndex);
        savedCheckRadioButton.setChecked(true);

        internetCheck = settingsPreferences.getBoolean(Constant.APP_PREFERENCES_INTERNET_CHECK, false);
    }
}
