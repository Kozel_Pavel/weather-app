package com.kozelpv.weatherapp.repository;

import com.kozelpv.weatherapp.repository.networking.models.WeatherDay;
import com.kozelpv.weatherapp.repository.networking.models.WeatherForecast;

import retrofit2.Call;

public interface RepositoryNetworking {
    Call<WeatherDay> getTodayWeatherByCityName(String cityName);

    Call<WeatherForecast> getForecastByCityName(String cityName);

    Call<WeatherDay> getTodayWeatherByCoordinates(Double latitude, Double longitude);

    Call<WeatherForecast> getForecastByCoordinates(Double latitude, Double longitude);
}
