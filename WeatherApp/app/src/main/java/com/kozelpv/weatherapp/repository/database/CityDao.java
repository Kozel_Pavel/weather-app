package com.kozelpv.weatherapp.repository.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CityDao {
    //Add City in DB
    @Insert
    void insert(City city);

    //Get city by Id
    @Query("SELECT * FROM city WHERE city_id = :cityId")
    City getCityById(int cityId);

    //Get all city from DB order by city_name
    @Query("SELECT * FROM city ORDER BY city_name ASC ")
    LiveData<List<City>> getAllCity();

    //Delete city by Id
    @Query("DELETE FROM city WHERE city_id = :cityId")
    void deleteCityDyId(int cityId);

    //Delete all city from DB
    @Query("DELETE FROM city")
    void deleteAllCity();
}
