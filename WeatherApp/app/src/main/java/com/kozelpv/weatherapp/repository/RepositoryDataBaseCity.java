package com.kozelpv.weatherapp.repository;

import androidx.lifecycle.LiveData;

import com.kozelpv.weatherapp.repository.database.City;

import java.util.List;

public interface RepositoryDataBaseCity {
    //Get all favorite city
    LiveData<List<City>> getAllCity();

    //Save favorite city in DB
    void insert(final City city);

    //Delete favorite city by Id
    void deleteById(final int id);
}
