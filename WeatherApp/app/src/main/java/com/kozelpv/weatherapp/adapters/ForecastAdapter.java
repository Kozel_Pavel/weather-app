package com.kozelpv.weatherapp.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.kozelpv.weatherapp.R;
import com.kozelpv.weatherapp.databinding.RecyclerViewDayForecastBinding;
import com.kozelpv.weatherapp.repository.networking.models.WeatherDay;
import com.kozelpv.weatherapp.repository.networking.models.WeatherForecast;

import java.util.ArrayList;
import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {

    private List<WeatherDay> dayForecastsArrayList = new ArrayList<>();

    public void setDataInAdapter(WeatherForecast weatherForecast) {
        dayForecastsArrayList.clear();
        dayForecastsArrayList.addAll(weatherForecast.getItems());
//        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ForecastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerViewDayForecastBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_view_day_forecast, parent, false);
        return new ForecastViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastViewHolder holder, int position) {
        holder.bind(dayForecastsArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return dayForecastsArrayList != null ? dayForecastsArrayList.size() : 0;
    }

    class ForecastViewHolder extends RecyclerView.ViewHolder {

        RecyclerViewDayForecastBinding binding;

        public ForecastViewHolder(@NonNull RecyclerViewDayForecastBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(WeatherDay weatherDay) {
            binding.setWeatherDay(weatherDay);
            binding.executePendingBindings();
        }
    }
}
